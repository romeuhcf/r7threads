module Blacklist
  def self.sanitize(msg)
    BADWORDS.each do |r|
      msg = sanitize_badword(msg, r)
    end

    msg
  end

  def self.sanitize_badword(original, regex)
    original.gsub(regex) do |found|
      'x' * found.size
    end
  end
end
