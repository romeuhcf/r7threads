require 'mongoid_auto_inc'

class Message
  include Mongoid::Document
  include Mongoid::Timestamps::Created

  field :text, type: String
  field :number, type: Integer

  has_many :child_messages, :class_name => 'Message'
  belongs_to :parent_message, :class_name => 'Message'

  auto_increment :number, scope: :parent_id

  def self.roots(page)
    self.where(parent_message_id: nil).page(page).per(10)
  end

  def root
    @_root ||= begin
                 current = self
                 loop do
                   return current if current.parent_message_id.nil?
                   current = current.parent_message
                 end
               end
  end

  def number_path
    @_number_path ||= begin
                        ids = []
                        current = self
                        loop do
                          ids << current.number
                          current = current.parent_message
                          break unless current
                        end
                        ids.reverse.join('.')
                      end
  end

  def child_message_list
    list= []
    child_messages.each do |m|
      list << m
      list = [list , m.child_message_list].flatten
    end
    list
  end
end
