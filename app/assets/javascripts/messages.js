$(function(){
  $('.btn.reply').click(replyForm);
});

function replyForm(evt){
  var btn = $(evt.target);
  btn.hide();
  var form_template = $('template').html();
  var message = btn.parent();
  var holder = btn.siblings('.formholder');
  var form = holder.append(form_template).find('form');
  var parent_id = btn.data('parent')
  var hidden = form.find('input[type=hidden][name="message[parent_message_id]"]')
  hidden.val(parent_id)
  $(form).addClass("well");
}
